//
//  VividSeatsServiceTests.swift
//  VividSeatsTests
//
//  Created by Spencer Müller Diniz on 18/05/18.
//  Copyright © 2018 VividSeats. All rights reserved.
//

import XCTest
import OHHTTPStubs

@testable import VividSeats

class VividSeatsServiceTests: XCTestCase {
    
    override func setUp() {
        super.setUp()

        stub(condition: isHost("webservices.vividseats.com") && isPath("/rest/mobile/v1/home/cards")) { _ in
            let stubPath = OHPathForFile("v1-home-cards.json", type(of: self))
            return fixture(filePath: stubPath!, headers: ["Content-Type": "application/json"])
        }
    }
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
    }
    
    func testHomeCards() {
        let homeCardsExpectation = XCTestExpectation(description: "homeCards")
        VividSeatsService().homeCards { (homeCards) in
            XCTAssertNotNil(homeCards, "homeCards must not be nil.")
            XCTAssertEqual(homeCards?.count, 3, "homeCards count should be 3.")
            XCTAssertNotNil(homeCards?.first, "homeCards should contain first item.")

            if let homeCard = homeCards?.first {
                XCTAssertEqual(homeCard.topLabel, "Vegas Golden Knights", "topLabel should be 'Vegas Golden Knights'.")
                XCTAssertEqual(homeCard.middleLabel, "T-Mobile Arena - Las Vegas, NV", "topLabel should be 'T-Mobile Arena - Las Vegas, NV'.")
                XCTAssertEqual(homeCard.bottomLabel, "Fri, May 18 - Sun, Jun 10", "topLabel should be 'Fri, May 18 - Sun, Jun 10'.")
                XCTAssertEqual(homeCard.eventCount, 6, "eventCount should be 6")
                XCTAssertEqual(homeCard.imageUrl?.absoluteString, "https://a.vsstatic.com/mobile/app/nhl/vegas-golden-knights.jpg", "image should be 'https://a.vsstatic.com/mobile/app/nhl/vegas-golden-knights.jpg'.")
            }
            
            homeCardsExpectation.fulfill()
        }
        
        self.wait(for: [homeCardsExpectation], timeout: 3.0)
    }
}
