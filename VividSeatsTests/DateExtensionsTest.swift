//
//  DateExtensionsTest.swift
//  VividSeatsTests
//
//  Created by Spencer Müller Diniz on 18/05/18.
//  Copyright © 2018 VividSeats. All rights reserved.
//

import XCTest
@testable import VividSeats

class DateExtensionsTest: XCTestCase {
    
    func testDateExtensions() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        
        let date = dateFormatter.date(from: "11-16-1979")!
        XCTAssertEqual(date.ymdFormatted, "1979-11-16", "date should be '1979-11-16'")
    }
    
}
