This is an iOS Swift project for a coding challenge for **VividSeats** and **BairesDev**.

To run the project, clone the repository, open in Xcode 9.3 and run. All required libraries are already included in the codebase. 

Cocoapods is used for dependency management and the following PODs are integrated:

 - **Alamofire:** Network library;
 - **AlamofireImage:** Alamofire extension for downloading images;
 - **ObjectMapper:** Library for mapping JSON objects;
 - **OHTTPStubs:** Library for mocking service responses for unit testing;

![Screenshot](https://bitbucket.org/spencerdiniz/vividseats/raw/master/screenshot.png)

