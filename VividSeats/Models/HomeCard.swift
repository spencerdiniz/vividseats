//
//  HomeCard.swift
//  VividSeats
//
//  Created by Spencer Müller Diniz on 17/05/18.
//  Copyright © 2018 VividSeats. All rights reserved.
//

import Foundation
import ObjectMapper

class HomeCard: Mappable {
    var topLabel: String?
    var middleLabel: String?
    var bottomLabel: String?
    var eventCount: Int?
    var imageUrl: URL?
    var startDate: Date?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.topLabel <- map["topLabel"]
        self.middleLabel <- map["middleLabel"]
        self.bottomLabel <- map["bottomLabel"]
        self.eventCount <- map["eventCount"]
        self.imageUrl <- (map["image"], URLTransform())
        
        var timeInterval: Double?
        timeInterval <- map["startDate"]
        if let timeInterval = timeInterval {
            self.startDate = Date(timeIntervalSince1970: timeInterval / 1000)
        }
    }
}


