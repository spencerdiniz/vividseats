//
//  HomeCardTableViewCell.swift
//  VividSeats
//
//  Created by Spencer Müller Diniz on 17/05/18.
//  Copyright © 2018 VividSeats. All rights reserved.
//

import UIKit
import AlamofireImage

class HomeCardTableViewCell: UITableViewCell {
    @IBOutlet private weak var cardView: UIView!
    @IBOutlet private weak var topLabel: UILabel!
    @IBOutlet private weak var middleLabel: UILabel!
    @IBOutlet private weak var bottomLabel: UILabel!
    @IBOutlet private weak var eventCountLabel: UILabel!
    @IBOutlet private weak var eventImageView: UIImageView!
    @IBOutlet private weak var chevronImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cardView.layer.cornerRadius = 5.0
        self.cardView.layer.borderColor = UIColor.lightGray.cgColor
        self.cardView.layer.borderWidth = 1.0
        self.chevronImageView.tintColorDidChange()
        self.prepareForReuse()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.topLabel.text = nil
        self.middleLabel.text = nil
        self.bottomLabel.text = nil
        self.eventCountLabel.text = nil
        self.eventImageView.image = nil
        
        self.eventImageView.af_cancelImageRequest()
        self.eventImageView.image = #imageLiteral(resourceName: "image-placeholder")
    }
    
    func setup(homeCard: HomeCard) {
        self.topLabel.text = homeCard.topLabel
        self.middleLabel.text = homeCard.middleLabel
        self.bottomLabel.text = homeCard.bottomLabel
        self.eventCountLabel.text = "\(homeCard.eventCount ?? 0) event(s)"
        
        if let imageUrl = homeCard.imageUrl {
            self.eventImageView.af_setImage(withURL: imageUrl)
        }
    }
}
