//
//  HomeViewController.swift
//  VividSeats
//
//  Created by Spencer Müller Diniz on 17/05/18.
//  Copyright © 2018 VividSeats. All rights reserved.
//

import UIKit

class HomeViewController: UITableViewController {

    private var homeCards = [HomeCard]() {
        didSet {
            self.tableView?.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        VividSeatsService().homeCards { [unowned self] (homeCards) in
            if let homeCards = homeCards {
                self.homeCards = homeCards
            }
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.homeCards.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCardCell", for: indexPath) as? HomeCardTableViewCell {
            let homeCard = self.homeCards[indexPath.row]
            cell.setup(homeCard: homeCard)
            
            return cell
        }

        return UITableViewCell()
    }
}
