//
//  VividSeatsService.swift
//  VividSeats
//
//  Created by Spencer Müller Diniz on 17/05/18.
//  Copyright © 2018 VividSeats. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class VividSeatsService {
    private let baseUrl = URL(string: "https://webservices.vividseats.com")
    
    func homeCards(completion: ((_ homeCards: [HomeCard]?) -> ())?) {
        guard let endpointUrl = URL(string: "/rest/mobile/v1/home/cards", relativeTo: self.baseUrl) else {
            completion?(nil)
            return
        }
        
        let headers: HTTPHeaders = [
            "X-Mobile-Platform": "iOS"
        ]
        
        let parameters: Parameters = [
            "startDate": Date().ymdFormatted,
            "endDate": Calendar.current.date(byAdding: .day, value: 30, to: Date())!.ymdFormatted,
            "includeSuggested": "true"
        ]
        
        let request = Alamofire.request(endpointUrl, method: .post, parameters: parameters, encoding: JSONEncoding(options: .prettyPrinted), headers: headers)
        
        request.responseJSON { (response) in
            if let json = response.result.value {
                let homeCards = Mapper<HomeCard>().mapArray(JSONObject: json)
                completion?(homeCards)
            }
            else {
                completion?(nil)
            }
        }
    }
}
