//
//  DateExtensions.swift
//  VividSeats
//
//  Created by Spencer Müller Diniz on 18/05/18.
//  Copyright © 2018 VividSeats. All rights reserved.
//

import Foundation

extension Date {
    var ymdFormatted: String {
        let isoDateFormatter = DateFormatter()
        isoDateFormatter.dateFormat = "yyyy-MM-dd"
        return isoDateFormatter.string(from: self)
    }
}
